package controllers

import (
	"badgesapp/app/models"
	"encoding/json"
	"errors"
	"github.com/revel/revel"
	"gopkg.in/mgo.v2/bson"
)

type BadgeController struct {
	*revel.Controller
}

func (c BadgeController) Index() revel.Result {
	var (
		badges []models.Badge
		err    error
	)
	badges, err = models.GetBadges()
	if err != nil {
		errResp := buildErrResponse(err, "500")
		c.Response.Status = 500
		return c.RenderJson(errResp)
	}
	c.Response.Status = 200
	return c.RenderJson(badges)
}

func (c BadgeController) Show(id string) revel.Result {
	var (
		badge   models.Badge
		err     error
		badgeID bson.ObjectId
	)

	if id == "" {
		errResp := buildErrResponse(errors.New("Invalid badge id format"), "400")
		c.Response.Status = 400
		return c.RenderJson(errResp)
	}

	badgeID, err = convertToObjectIdHex(id)
	if err != nil {
		errResp := buildErrResponse(errors.New("Invalid badge id format"), "400")
		c.Response.Status = 400
		return c.RenderJson(errResp)
	}

	badge, err = models.GetBadge(badgeID)
	if err != nil {
		errResp := buildErrResponse(err, "500")
		c.Response.Status = 500
		return c.RenderJson(errResp)
	}

	c.Response.Status = 200
	return c.RenderJson(badge)
}

func (c BadgeController) Create() revel.Result {
	var (
		badge models.Badge
		err   error
	)

	err = json.NewDecoder(c.Request.Body).Decode(&badge)
	if err != nil {
		errResp := buildErrResponse(err, "403")
		c.Response.Status = 403
		return c.RenderJson(errResp)
	}

	badge, err = models.AddBadge(badge)
	if err != nil {
		errResp := buildErrResponse(err, "500")
		c.Response.Status = 500
		return c.RenderJson(errResp)
	}
	c.Response.Status = 201
	return c.RenderJson(badge)
}

func (c BadgeController) Update() revel.Result {
	var (
		badge models.Badge
		err   error
	)
	err = json.NewDecoder(c.Request.Body).Decode(&badge)
	if err != nil {
		errResp := buildErrResponse(err, "400")
		c.Response.Status = 400
		return c.RenderJson(errResp)
	}

	err = badge.UpdateBadge()
	if err != nil {
		errResp := buildErrResponse(err, "500")
		c.Response.Status = 500
		return c.RenderJson(errResp)
	}
	return c.RenderJson(badge)
}

func (c BadgeController) Delete(id string) revel.Result {
	var (
		err     error
		badge   models.Badge
		badgeID bson.ObjectId
	)
	if id == "" {
		errResp := buildErrResponse(errors.New("Invalid badge id format"), "400")
		c.Response.Status = 400
		return c.RenderJson(errResp)
	}

	badgeID, err = convertToObjectIdHex(id)
	if err != nil {
		errResp := buildErrResponse(errors.New("Invalid badge id format"), "400")
		c.Response.Status = 400
		return c.RenderJson(errResp)
	}

	badge, err = models.GetBadge(badgeID)
	if err != nil {
		errResp := buildErrResponse(err, "500")
		c.Response.Status = 500
		return c.RenderJson(errResp)
	}
	err = badge.DeleteBadge()
	if err != nil {
		errResp := buildErrResponse(err, "500")
		c.Response.Status = 500
		return c.RenderJson(errResp)
	}
	c.Response.Status = 204
	return c.RenderJson(nil)
}
