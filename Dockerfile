FROM golang:1.7.3

# Move current project to a valid go path
WORKDIR /go/src/badgesapp

# Install Revel CLI
RUN go get github.com/revel/revel \
	&& go get github.com/revel/cmd/revel \
	&& go get github.com/kyawmyintthein/revel_mgo

# Run app in production mode
EXPOSE 9000
ENTRYPOINT revel run badgesapp prod 9000
